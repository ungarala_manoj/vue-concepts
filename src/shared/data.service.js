import * as axios from 'axios';

import { format } from 'date-fns';
import { inputDateFormat } from './constants';

import { API } from './config';

const getDoctors = async function() {
  try {
     const response = await axios.get(`${API}/doctors.json`);

    let data = parseList(response);

    const doctors = data.map(h => {
      h.originDate = format(h.originDate, inputDateFormat);
      return h;
    });
    return doctors;
  } catch (error) {
    console.error(error);
    return [];
  }
};

// const getDoctor = async function(id) {
//   try {
//     const response = await axios.get(`${API}/doctors/${id}`);
//     let doctor = parseItem(response, 200);
//     return doctor;
//   } catch (error) {
//     console.error(error);
//     return null;
//   }
// };
const getDoctor = async function(id) {
  try {
    var response = await axios.get(`${API}/doctors.json`);
    //let doctor = parseItem(response, 200);
    if(response){
    var doctor = response.data.filter(x=> x.id == id);
    }
    return doctor;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const updateDoctor = async function(doctor) {
  try {
    const response = await axios.put(`${API}/doctors/${doctor.id}`, doctor);
    const updatedDoctor = parseItem(response, 200);
    return updatedDoctor;
  } catch (error) {
    console.error(error);
    return null;
  }
};

const parseList = response => {
  if (response.status !== 200) throw Error(response.message);
  if (!response.data) return [];
  let list = response.data;
  if (typeof list !== 'object') {
    list = [];
  }
  return list;
};

export const parseItem = (response, code) => {
  if (response.status !== code) throw Error(response.message);
  let item = response.data;
  if (typeof item !== 'object') {
    item = undefined;
  }
  return item;
};

export const dataService = {
  getDoctors,
  getDoctor,
  updateDoctor,
};
